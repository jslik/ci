#!/usr/bin/env python

import os, time, sys
import pickle

os.chdir('/home/jesper/Documents/Uni/CI/torcs-client/')
sys.path.insert(0, '/home/jesper/Documents/Uni/CI/torcs-client/NEAT/src')
from peas.methods.neat import NEATPopulation, NEATGenotype, NEATSpecies
from peas.tasks.xor import XORTask
from peas.tasks.torcs import TorcsTask
from peas.networks.rnn import NeuralNetwork

loc_best = '/home/jesper/Documents/Uni/CI/torcs-client/networks/best.pickle'
loc_add = '/home/jesper/Documents/Uni/CI/torcs-client/results/run3_best.pickle'


genotype = lambda innovations={}: NEATGenotype(inputs=10,
                                               outputs=2,
                                               response_default=.5,
                                               initial_weight_stdev=5.0,
                                               prob_add_node=0.005,
                                               prob_add_conn=0.1,
                                               prob_mutate_weight=0.7,
                                               prob_reset_weight=0.03,
                                               prob_reenable_conn=0.01,
                                               prob_disable_conn=0.1,
                                               stdev_mutate_weight=.5,
                                               stdev_mutate_bias=0.5,
                                               stdev_mutate_response=.5,
                                               weight_range=(-5., 5.),
                                               types=['sigmoid3'],
                                               innovations=innovations,
                                               )

task = TorcsTask()

popSize = 100
pop = NEATPopulation(genotype, popsize=popSize, sim_geno_loc=loc_best, run_multiple=True)

with open('/home/jesper/Documents/Uni/CI/torcs-client/results/run6_best.pickle', 'rb') as f:
    geno_add = pickle.load(file=f)

pop._reset()
pop.species = []
pop.species.append(NEATSpecies(geno_add, 0))

pop.epoch(generations=1000, evaluator=task, solution=task, reset=True)
