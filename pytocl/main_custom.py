"""Application entry point."""
import argparse
import logging
import os, time

from my_driver import MyDriver
from pytocl.protocol import Client

# dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path_up = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))

loc_log = dir_path_up + '/logs/0000'
loc_network = dir_path_up + '/networks/nn_1.pickle'


def main():
    """Main entry point of application."""
    parser = argparse.ArgumentParser(
        description='Client for TORCS racing car simulation with SCRC network'
                    ' server.'
    )
    parser.add_argument(
        '--hostname',
        help='Racing server host name.',
        default='localhost'
    )
    parser.add_argument(
        '-p',
        '--port',
        help='Port to connect, 3001 - 3010 for clients 1 - 10.',
        type=int,
        default=3001
    )
    parser.add_argument(
        '-t',
        '--time',
        help='Maximum simulation evaluation time in seconds (so simulation seconds)',
        type=int,
        default=float("inf")
    )
    parser.add_argument(
        '-o',
        '--output',
        help='Location of the output folder, in which results will be logged',
        type=str,
        default=loc_log
    )
    parser.add_argument(
        '-n',
        '--nnfile',
        help='Location of the file in which the neural network is stored (.pickle file)',
        type=str,
        default=loc_network
    )
    parser.add_argument(
        '-i',
        '--identifier',
        help='Identifying name of the controller',
        type=str,
        default='default'
    )
    parser.add_argument('-v', help='Debug log level.', action='store_true')
    args = parser.parse_args()

    # switch log level:
    if args.v:
        level = logging.DEBUG
    else:
        level = logging.INFO
    del args.v
    logging.basicConfig(
        level=level,
        format="%(asctime)s %(levelname)7s %(name)s %(message)s"
    )

    driver = MyDriver(nnfile=args.nnfile, logdata=False, evaltime=args.time, logfile=args.output, identifier=args.identifier)
    del args.time
    del args.output
    del args.nnfile
    del args.identifier

    # start client loop:
    client = Client(driver=driver, **args.__dict__)
    client.run()


if __name__ == '__main__':
    print(os.getcwd())
    main()