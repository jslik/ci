import sys, os, time, math #, pickle
import numpy as np

from builtins import min

from pytocl.driver import Driver
from pytocl.car import State, Command
from peas.networks.rnn import NeuralNetwork

from pytocl.analysis import DataLogWriter
from pytocl.car import State, Command, MPS_PER_KMH
from pytocl.controller import CompositeController, ProportionalController, \
    IntegrationController, DerivativeController


class MyDriver(Driver):
    def __init__(self, nnfile='temp', logdata=True, evaltime=float("inf"), logfile='temp', identifier='default'):
        self.steering_ctrl = CompositeController(
            ProportionalController(0.4),
            IntegrationController(0.2, integral_limit=1.5),
            DerivativeController(2)
        )
        self.acceleration_ctrl = CompositeController(
            ProportionalController(3.7),
        )
        self.data_logger = DataLogWriter() if logdata else None

        self.stopped = False
        self.startPos = None
        self.startdamage = None
        self.evalTime = evaltime
        self.identifier = identifier
        self.logger = CustomLogger(logfile)

        self.d_time = 0
        self.d_meters = 0

        self.m_edge = self.get_edge_matrix()
        self.m_opp = self.get_opp_matrix()

        self.nn = self.init_network()

        # with open(nnfile, 'rb') as file:
        #     self.nn = pickle.load(file=file, encoding='latin1')
        #     self.nn.make_feedforward()
        # self.logger.__write2__('started')

    def drive(self, carstate: State) -> Command:
        self.do_checks(carstate)

        inputs = self.normalize_input(carstate)
        outputs = self.nn.feed(inputs)[-2:]

        command = Command()
        self.steer(carstate, outputs[0], command)
        self.accelerate(carstate, outputs[1], command)

        return command

    def do_checks(self, carstate):
        self.startPos = carstate.distance_raced if not self.startPos else self.startPos
        self.startdamage = carstate.damage if not self.startdamage else self.startdamage

        driven_lastsecond = 1
        if carstate.current_lap_time > 0 and carstate.last_lap_time + carstate.current_lap_time > self.d_time:
            self.d_time = self.d_time + 1
            driven_lastsecond = carstate.distance_raced - self.d_meters
            self.d_meters = carstate.distance_raced

        checkTime = carstate.current_lap_time > 0 and carstate.last_lap_time + carstate.current_lap_time > self.evalTime
        checkTrack = abs(carstate.distance_from_center) > 1
        checkDamage = carstate.damage - self.startdamage > 1000
        checkDriven = driven_lastsecond <= 0 and carstate.current_lap_time > 3

        if checkTime or checkTrack or checkDamage or checkDriven:
            # self.logger.__write2__(str(carstate.damage) + "\t" + str(self.startdamage))
            # self.logger.__write2__('Stopping...\t' + str([checkTime, checkTrack, checkDamage, checkDriven]))
            self.stop_evaluation(carstate)

    def steer(self, carstate, value, command):
        command.steering = self.steering_ctrl.control(
            value,
            carstate.current_lap_time
        )

    def accelerate(self, carstate, value, command):
        acceleration = self.acceleration_ctrl.control(
            value,
            carstate.current_lap_time
        )
        command.accelerator = acceleration if acceleration > 0 else 0
        command.brake = abs(acceleration) if acceleration < 0 else 0

        command.gear = carstate.gear if carstate.gear > 0 else 1
        if carstate.rpm > 8000 and acceleration > 0:
            command.gear = carstate.gear + 1
        elif carstate.rpm < 2500 and carstate.gear > 1:
            command.gear = carstate.gear - 1


    def normalize_input(self, carstate):
        a_n = carstate.angle/math.pi                                     # [-pi, pi]
        s_n = (max(0, min(carstate.speed_x, 200)) / 100) - 1             # [-inf, inf]
        c_n = max(min(carstate.distance_from_center / 2, 1), -1)         # [-1, 1]
        e_n = [((x + 1)/100.5)-1 for x in carstate.distances_from_edge]  # [-1, 200]
        # e_n = [e_n[i] for i in [8, 9, 10]]
        e_n = np.dot(e_n, self.m_edge) / 3.5
        e_n = np.squeeze(np.asarray(e_n))

        # d_n = [((x + 1) / 100.5) - 1 for x in carstate.opponents]        # [-1, 200]
        # d_n = [d_n[i] for i in [16, 18, 20]]
        # d_n = np.dot(d_n, self.m_opp) / 7
        # d_n = np.squeeze(np.asarray(d_n))

        return np.array([a_n] + [s_n] + [c_n] + e_n.tolist(), dtype=float)

    def stop_evaluation(self, carstate):
        """ Stops the program by crashing it """
        # if not self.stopped:
        #     self.logger.__write__(str(self.identifier) + "\t" + str(carstate.distance_raced + 1))
        # self.stopped = True

    def get_edge_matrix(self):
        return(np.matrix([
            [0.00382193, -0.043541543, -0.026791143, -0.054016177],
            [-0.003755029, -0.053264211, -0.006949588, -0.04533208],
            [-0.009094315, -0.064467505, 0.003483263, -0.045054891],
            [-0.014708092, -0.086260081, 0.013227898, -0.052822394],
            [-0.023479008, -0.14820041, 0.03333432, -0.083796193],
            [-0.029905763, -0.327314084, 0.086683261, -0.182413565],
            [-0.051114286, -0.423753811, 0.129439645, -0.140648636],
            [-0.108938396, -0.516779884, 0.184236153, 0.084517455],
            [-0.271276549, -0.436251886, 0.076333857, 0.277071585],
            [-0.86327886, 0.136864379, -0.380374734, 0.156249982],
            [-0.354434677, 0.221307357, 0.402373507, -0.643339487],
            [-0.137553304, 0.168567783, 0.44008071, -0.103080214],
            [-0.063347782, 0.173879843, 0.403213172, 0.25579714],
            [-0.026030245, 0.174394669, 0.351755561, 0.405253658],
            [0.002371761, 0.133850862, 0.186948303, 0.337637811],
            [0.01018943, 0.087511081, 0.06656654, 0.178932751],
            [0.011775006, 0.064359609, 0.012194587, 0.074804666],
            [0.101285617, 0.141271467, -0.277502987, -0.114758699],
            [0.071852602, 0.105746142, -0.17546108, -0.053359056],
        ]))

    def get_opp_matrix(self):
        return(np.matrix([
            [-0.295158275,-0.481570488,-0.421566677,-0.345695076,-0.164541728,0.185037522,0.061847964,0.491541772,0.023264853,-0.14027241],
            [-0.105516399,-0.373633222,-0.052128198,0.197518291,0.029619536,-0.216159619,0.625821909,-0.050292972,0.168779495,0.217950938],
            [-0.051311911,-0.227308633,-0.051454683,-0.085880525,0.134244652,0.319877104,-0.316300137,-0.178027276,0.129868369,0.348581146],
            [0.010227068,-0.008000307,0.000534412,-0.007804565,0.041944356,-0.013586318,0.004769236,0.002099911,-0.016328224,0.014825982],
            [0.012405703,-0.006053109,-0.002804431,0.003007646,0.03130157,-0.031872547,-0.001445826,0.004780924,-0.011316162,-0.018178207],
            [0.028970298,0.003209413,-0.003848121,0.00402125,0.013989118,-0.019862063,-0.016400443,-0.016119033,-0.007105148,-0.02730517],
            [0.063228751,0.000962817,0.002664852,0.00903558,0.038821714,-0.038945881,-0.03439733,-0.035551813,-0.003597143,-0.036189843],
            [0.155256818,0.020781955,0.045547541,-0.005633565,-0.248560318,0.134358259,0.021079574,-0.020181734,0.034415171,0.05505494],
            [0.293451859,0.049298882,0.093702902,-0.012371095,-0.516466457,0.256479086,0.153647959,0.040502399,0.039365674,0.113243371],
            [0.021596855,-2.72E-05,0.005146745,0.009252437,-0.019605646,-0.001246922,-0.005669801,0.004618985,0.003756298,-0.015561007],
            [0.011897273,-0.001688518,0.002114877,0.00905227,-0.004134268,-0.004946525,-0.00932091,0.011127096,-0.001383262,-0.018688691],
            [0.007098697,-0.003255737,0.001563467,0.010030555,0.016207277,-0.018073144,-0.013627127,0.00268808,-0.00166611,-0.028738368],
            [0.0052698,-0.009170648,-0.003624402,0.001903576,0.020597919,-0.012046102,-0.002970772,0.002450053,-0.001668521,-0.01742088],
            [0.004428545,-0.014854098,-0.004179547,0.004100271,0.015575822,-0.017895586,0.010183003,0.012621583,-0.004138928,-0.024220055],
            [0.003776436,-0.0205372,3.53E-05,0.005477514,0.017172355,-0.02205226,0.00602463,0.021541008,-0.007119553,-0.038304557],
            [-0.070065418,-0.244019138,0.032675252,-0.0619826,0.088901612,0.356263879,-0.346303191,-0.260448949,0.161611962,0.270222482],
            [-0.096206972,-0.252199788,0.039064393,0.285248186,-0.03234633,-0.124486287,0.164849464,-0.327642896,0.17014006,0.280344221],
            [-0.475479641,-0.162407086,0.404019208,0.488294735,-0.3548874,-0.082194133,-0.329000917,0.237823536,-0.055361185,-0.079164953],
            [-0.441354548,0.0499566,0.50987045,-0.450434858,0.014848537,0.258376979,0.341055292,-0.305806334,-0.108059411,-0.209431303],
            [-0.207359424,0.217835612,0.080201698,-0.440909773,-0.215814996,-0.515513508,-0.163158568,0.117749443,0.011605865,0.513866247],
            [-0.12152264,0.225995833,0.004973603,0.073686803,0.191283203,0.231671633,0.213006362,0.313022542,-0.210745972,0.503217268],
            [-0.002048914,0.018374186,-0.007081048,-0.067395092,0.059821877,-0.041653535,0.025830335,0.032705156,-0.067860705,0.156040322],
            [0.012795264,0.005679475,0.002766673,-0.020974472,0.049291293,-0.023430292,0.003167429,-0.003086903,-0.0217154,0.034716772],
            [0.018814571,0.008297115,-0.006929935,-0.008729572,0.021908296,-0.021009193,-0.014081863,-0.012846967,-0.009159731,0.01273352],
            [0.038617003,0.011572964,0.006912369,-0.005344861,-0.036182892,0.006091912,-0.004912042,-0.013880377,0.007347403,0.005588508],
            [0.068996301,0.013617032,0.025193304,-0.006255567,-0.124865541,0.044037415,0.017148581,-0.011651425,0.014532936,0.03790348],
            [0.227095652,0.043269978,0.077477566,-0.016595101,-0.470143085,0.233750022,0.133144869,0.027810183,0.049001795,0.128298639],
            [0.050744311,0.009574639,0.015325345,-0.006175424,-0.075798156,0.032408449,0.012613526,0.008947563,0.006573742,0.003068239],
            [0.013818318,0.004215871,0.004512908,-0.005757206,-0.0210347,0.007035763,0.001697552,0.000800142,0.001177747,-0.007427624],
            [0.00487846,0.001963799,0.00081416,-0.003385897,0.009239029,-0.008178824,-0.007024929,-0.002674585,-0.00278494,-0.016940117],
            [-0.000135846,0.004734138,-0.006309614,-0.001777751,0.006069315,-0.006838927,-0.00252612,-0.010738698,0.007776116,-0.015278223],
            [-0.002113919,0.009267516,-0.007466452,0.007575377,0.015094364,-0.001000269,-0.006509466,-0.007616769,0.002798354,-0.023285678],
            [-0.002902314,0.020702986,0.001902995,0.024792235,0.04293388,0.000360287,-0.012553419,0.000586261,0.029341854,-0.044925758],
            [-0.131858512,0.265313654,0.05341892,0.276866632,0.242360997,0.349022311,0.116735057,0.301576327,-0.115456555,0.111952046],
            [-0.226847085,0.395434765,-0.09395245,0.020500001,0.001424734,0.072170094,0.039708186,0.091054517,0.857454507,-0.102976943],
            [-0.400350198,0.304164301,-0.595727101,0.154393365,-0.299354704,0.074318551,0.017223403,-0.425237876,-0.281250086,-0.011032256],
        ]))

    def get_cm(self):
        return np.squeeze(np.asarray(np.matrix([
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0.46438732, 0., -0.34404382, 0., -0.78994521, 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [-0.12377771, -0.10802863, 0., 0., 0., 0., -0.48720158, 0., 0., 0., 0., 0.],
            [-0.44656622, 0., 0., -2.5, 0., 0., -0.24687547, 0., 1.08226765, -0.32744625, 0., 0.],
            [-0.69849272, 0., -2.4723445, 0., -2.01274512, 0., 0.76655592, 0., -0.06136649, 0., 0., 0.]
        ])))

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def sigmoid3(self, x):
        return (self.sigmoid(x) * 2) - 1

    def init_network(self):
        """ creates the best trained network """
        nn = NeuralNetwork()
        nn.original_shape = (12,)
        nn.cm = self.get_cm()
        nn.node_types = [self.sigmoid3] * 12
        nn.act = np.array([0.] * 12)
        nn.sum_all_node_inputs = True
        nn.all_nodes_same_function = True
        nn.feedforward = True
        nn.sandwich = False
        return nn


class CustomLogger():
    def __init__(self, filename):
        self.fileName = filename

    def __write2__(self, msg):
        mode = 'a' if os.path.isfile(self.fileName + '2') else 'w'
        with open(self.fileName + '2', mode) as log:
            log.write(msg + "\n")

    def __write__(self, msg):
        mode = 'a' if os.path.isfile(self.fileName) else 'w'
        with open(self.fileName, mode) as log:
            log.write(msg + "\n")

    def overwrite(self, msg):
        with open(self.fileName, 'w') as log:
            log.write(msg + "\n")
