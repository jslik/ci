import numpy as np
import sys, pickle

# sys.path.insert(0, '/home/jesper/Documents/Uni/CI/torcs-client/NEAT/src')
from peas.networks.rnn import NeuralNetwork

loc_network = '/home/jesper/Documents/Uni/CI/torcs-client/ci/nn_best.pickle'

with open(loc_network, 'rb') as file:
    nn = pickle.load(file=file, encoding='latin1')


def get_cm():
    return np.squeeze(np.asarray(np.matrix([
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0.46438732, 0., -0.34404382, 0., -0.78994521, 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        [-0.12377771, -0.10802863, 0., 0., 0., 0., -0.48720158, 0., 0., 0., 0., 0.],
        [-0.44656622, 0., 0., -2.5, 0., 0., -0.24687547, 0., 1.08226765, -0.32744625, 0., 0.],
        [-0.69849272, 0., -2.4723445, 0., -2.01274512, 0., 0.76655592, 0., -0.06136649, 0., 0., 0.]
    ])))

def sigmoid(x):
    """ Sigmoid function.
    """
    return 1 / (1 + np.exp(-x))

def sigmoid3(x):
    """ Sigmoid scaled [-1,1]
    """
    return(sigmoid(x) * 2) - 1

def init_network():
    nn = NeuralNetwork()
    nn.original_shape = (12,)
    nn.cm = get_cm()
    nn.node_types = [sigmoid3]*12
    nn.act = np.array([0.]*12)
    nn.sum_all_node_inputs = True
    nn.all_nodes_same_function = True
    nn.feedforward = True
    nn.sandwich = False
    return nn

nn2 = init_network()

# nn2.original_shape = nn.original_shape
# nn2.cm = nn.cm
# nn2.node_types = nn.node_types
# nn2.act = nn.act
# nn2.sum_all_node_inputs = nn.sum_all_node_inputs
# nn2.all_nodes_same_function = nn.all_nodes_same_function
# nn2.feedforward = nn.feedforward
# nn2.sandwich = nn.sandwich

i = np.array([.3, .2, -.8])
print(nn.feed(i))
print(nn2.feed(i))


