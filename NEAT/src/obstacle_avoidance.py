# -*- coding: utf-8 -*-

from parameters import *
# from neat_task import NEATTask

import numpy as np
import dbus
import dbus.mainloop.glib
import logging
logging.basicConfig()
import parameters as pr
from helpers import *
from task_evaluator import TaskEvaluator
from peas.methods.hyperneat import Substrate
import thread
import thymio_task

EVALUATIONS = 1000
TIME_STEP = 0.005
ACTIVATION_FUNC = 'tanh'
POPSIZE = 10
GENERATIONS = 30
TARGET_SPECIES = 2
SOLVED_AT = EVALUATIONS * 2
EXPERIMENT_NAME = 'obstacle_avoidance'

CURRENT_FILE_PATH = os.path.abspath(os.path.dirname(__file__))
MAIN_LOG_PATH = os.path.join(CURRENT_FILE_PATH, 'log_main')
OUTPUT_PATH = os.path.join(CURRENT_FILE_PATH, 'output')
PICKLED_DIR = os.path.join(CURRENT_FILE_PATH, 'pickled')
FORMATTER = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s')
AESL_PATH = os.path.join(CURRENT_FILE_PATH, 'asebaCommands.aesl')


class ObstacleAvoidance(TaskEvaluator):

    def __init__(self, debug=False, experimentName=EXPERIMENT_NAME, evaluations=EVALUATIONS, timeStep=0.005, activationFunction='tanh', popSize=POPSIZE, generations=GENERATIONS, solvedAt=1000):
        TaskEvaluator.__init__(self, debug, experimentName, evaluations, timeStep, activationFunction, popSize, generations, solvedAt)
        self.ctrl_thread_started = False
        self.hitWallCounter = 0
        self.atWall = False
        print "New obstacle avoidance task"

    def evaluate(self, evaluee):
        #print "thread started:", self.ctrl_thread_started
        if self.ctrl_client and not self.ctrl_thread_started:
            thread.start_new_thread(check_stop, (self, ))
            self.ctrl_thread_started = True

        return TaskEvaluator.evaluate(self, evaluee)

    def _step(self, evaluee, callback):
        def ok_call(psValues):
            psValues = np.array([psValues[0], psValues[2], psValues[4], psValues[5], psValues[6], 1],dtype='f')
            psValues[0:5] = [(float(x) - float(pr.SENSOR_MAX[0]/2))/float(pr.SENSOR_MAX[0]/2) for x in psValues[0:5]]
            left, right = list(evaluee.feed(psValues)[-2:])
            motorspeed = { 'left': left, 'right': right }
            try:
                writeMotorSpeed(self.thymioController, motorspeed)
            except Exception as e:
                print str(e)

            # print "Sensor values: ", psValues, " sensor max: ", SENSOR_MAX
            if not self.atWall and any(i >= 1 for i in psValues[0:5]):
                self.atWall = True
                self.hitWallCounter += 1
            elif not any(i >= 1 for i in psValues[0:5]):
                self.atWall = False

            callback(self.getFitness(motorspeed, psValues))

        def nok_call():
            print " Error while reading proximity sensors"

        getProxReadings(self.thymioController, ok_call, nok_call)
        return True

    def getFitness(self, motorspeed, observation):
        # Calculate penalty for rotating
        # speedpenalty = 0
        # if motorspeed['left'] > motorspeed['right']:
        #     speedpenalty = float((motorspeed['left'] - motorspeed['right']))
        # else:
        #     speedpenalty = float((motorspeed['right'] - motorspeed['left']))

        speedpenalty = float(abs(motorspeed['left'] - motorspeed['right']))

        # Calculate normalized distance to the nearest object
        sensorpenalty = 0
        for i, sensor in enumerate(observation[:-1]):
            distance = sensor
            if sensorpenalty < distance:
                sensorpenalty = distance


        # fitness for 1 timestep in [-2, 2]
        return float(motorspeed['left'] + motorspeed['right']) * (1 - min(speedpenalty,1)) * (1 - min(sensorpenalty,1))

if __name__ == '__main__':
    task = thymio_task.ThymioTask(ObstacleAvoidance(experimentName=EXPERIMENT_NAME), use_img_client=False)
    task.start()