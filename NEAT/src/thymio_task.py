import base64
from json import JSONEncoder

from peas.methods.neat import NEATPopulation, NEATGenotype
import peas.methods.hyperneat as hn
from helpers import *
from parameters import *
from cameravision import *

import dbus
import dbus.mainloop.glib
from copy import deepcopy
import json
import time
import sys
import socket
import thread

from peas.networks import NeuralNetwork

CURRENT_FILE_PATH = os.path.abspath(os.path.dirname(__file__))
AESL_PATH = os.path.join(CURRENT_FILE_PATH, 'asebaCommands.aesl')


class CustomEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            if obj.flags['C_CONTIGUOUS']:
                obj_data = obj.data
            else:
                cont_obj = np.ascontiguousarray(obj)
                assert (cont_obj.flags['C_CONTIGUOUS'])
                obj_data = cont_obj.data
            data_b64 = base64.b64encode(obj_data)
            return dict(__ndarray__=data_b64,
                        dtype=str(obj.dtype),
                        shape=obj.shape)
        return obj.__dict__

class ThymioTask:
    def __init__(self, evaluator, use_img_client=False):
        self.evaluator = evaluator
        self.use_odneat = '-o' in sys.argv
        self.use_hyperneat = '-h' in sys.argv
        if self.use_odneat:
            if self.use_hyperneat:
                self.method = 'odHyperNEAT'
            else:
                self.method = 'odNEAT'
        else:
            if self.use_hyperneat:
                self.method = 'HyperNEAT'
            else:
                self.method = 'NEAT'
        print 'Starting ' + self.method
        self.experiment_name = self.method + '_' + evaluator.experimentName
        self.use_img_client = use_img_client
        self.ip = sys.argv[1]
        self.commit_sha = sys.argv[2]

        mkdir_p(os.path.join(OUTPUT_PATH, self.experiment_name))
        mkdir_p(PICKLED_DIR)

        if self.use_hyperneat:
            evaluator._use_hyperneat = True

    def start(self,
              EVALUATIONS=1000,
              MAX_MOTOR_SPEED=150,

              ACTIVATION_FUNC='tanh',
              POPSIZE=20,
              GENERATIONS=100,
              TARGET_SPECIES=8):
        genotype = lambda innovations={}: NEATGenotype(
            inputs=6,
            outputs=2,
            types=[ACTIVATION_FUNC],
            prob_add_node=0.1,
            weight_range=(-3, 3),
            stdev_mutate_weight=.25,
            stdev_mutate_bias=.25,
            stdev_mutate_response=.25,
            feedforward=False,
            use_odneat=self.use_odneat,
            innovations=innovations)
        pop = NEATPopulation(genotype, popsize=POPSIZE, target_species=TARGET_SPECIES, stagnation_age=5,
                             ip_address=self.ip, use_odneat=self.use_odneat, use_hyperneat=self.use_hyperneat)

        log = {'neat': {}, 'generations': []}

        # log neat settings
        dummy_individual = genotype()
        log['neat'] = {
            'max_speed': MAX_MOTOR_SPEED,
            'evaluations': EVALUATIONS,
            'activation_function': ACTIVATION_FUNC,
            'popsize': POPSIZE,
            'generations': GENERATIONS,
            'elitism': pop.elitism,
            'tournament_selection_k': pop.tournament_selection_k,
            'target_species': pop.target_species,
            'stagnation_age': pop.stagnation_age,
            'feedforward': dummy_individual.feedforward,
            'initial_weight_stdev': dummy_individual.initial_weight_stdev,
            'prob_add_node': dummy_individual.prob_add_node,
            'prob_add_conn': dummy_individual.prob_add_conn,
            'prob_mutate_weight': dummy_individual.prob_mutate_weight,
            'prob_reset_weight': dummy_individual.prob_reset_weight,
            'prob_reenable_conn': dummy_individual.prob_reenable_conn,
            'prob_disable_conn': dummy_individual.prob_disable_conn,
            'prob_reenable_parent': dummy_individual.prob_reenable_parent,
            'stdev_mutate_weight': dummy_individual.stdev_mutate_weight,
            'stdev_mutate_bias': dummy_individual.stdev_mutate_bias,
            'stdev_mutate_response': dummy_individual.stdev_mutate_response,
            'weight_range': dummy_individual.weight_range
        }
        log['neat'].update(self.evaluator.logs())

        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        bus = dbus.SessionBus()
        thymioController = dbus.Interface(bus.get_object('ch.epfl.mobots.Aseba', '/'),
                                          dbus_interface='ch.epfl.mobots.AsebaNetwork')
        thymioController.LoadScripts(AESL_PATH, reply_handler=dbusReply, error_handler=dbusError)

        # switch thymio LEDs off
        thymioController.SendEventName('SetColor', [0, 0, 0, 0], reply_handler=dbusReply, error_handler=dbusError)

        task = self.evaluator
        task.set_thymio_controller(thymioController)

        ctrl_serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ctrl_serversocket.bind((self.ip, 1337))
        ctrl_serversocket.listen(5)
        ctrl_client = None
        img_serversocket = None
        img_client = None

        def set_client():
            global ctrl_client
            print 'Control server: waiting for socket connections...'
            (ctrl_client, address) = ctrl_serversocket.accept()
            task.set_ctrl_client(ctrl_client)
            print 'Control server: got connection from', address

        thread.start_new_thread(set_client, ())

        if self.use_img_client:
            img_serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            img_serversocket.bind((self.ip, 31337))
            img_serversocket.listen(5)

            def set_img_client():
                global img_client
                print 'Image server: waiting for socket connections...'
                (img_client, address) = img_serversocket.accept()
                print 'Image server: got connection from', address
                write_header(img_client)

            thread.start_new_thread(set_img_client, ())

        def epoch_callback(population):
            # update log
            population_backup = population.giveBackUp()
            species_backup = population.giveBackUpSpecies()
            generation = {'individuals': [], 'phenotypes': [], 'gen_number': population.generation}
            for individual in population_backup:
                copied_connections = {str(key): value for key, value in individual.conn_genes.items()}
                generation['individuals'].append({
                    'node_genes': deepcopy(individual.node_genes),
                    'conn_genes': copied_connections,
                    'stats': deepcopy(individual.stats)
                })
            for phenotype in population.phenotype_backup:
                generation['phenotypes'].append({
                    'cm': deepcopy(phenotype.cm),
                    'act': deepcopy(phenotype.act)
                })
            champion_file = self.experiment_name + '_{}_{}.p'.format(self.commit_sha, population.generation)
            generation['champion_file'] = champion_file
            generation['species'] = [len(species.members) for species in species_backup]
            print generation['species']
            log['generations'].append(generation)

            task.getLogger().info(', '.join([str(ind.stats['fitness']) for ind in population.population]))

            outputDir = os.path.join(OUTPUT_PATH, self.experiment_name)
            date = time.strftime("%d-%m-%y_%H-%M")
            jsonLogFilename = os.path.join(outputDir, self.experiment_name + '_' + date + '.json')

            print(jsonLogFilename)
            with open(jsonLogFilename, 'w') as f:
                json.dump(log, f, cls=CustomEncoder)

            current_champ = population_backup[-1]
            pickle.dump(current_champ, file(os.path.join(PICKLED_DIR, champion_file), 'w'))

        try:
            evaluator = lambda evaluee: task.evaluate(NeuralNetwork(evaluee)) \
                if hasattr(evaluee, 'get_network_data') \
                else task.evaluate(evaluee)
            converter = hn.create_converter(task.substrate()) if self.use_hyperneat else lambda x: x
            pop.epoch(generations=GENERATIONS, evaluator=evaluator, solution=evaluator, callback=epoch_callback, converter=converter)
        except KeyboardInterrupt:
            release_resources(task.thymioController, ctrl_serversocket, ctrl_client, img_serversocket, img_client)
            sys.exit(1)

        release_resources(task.thymioController, ctrl_serversocket, ctrl_client, img_serversocket, img_client)
        sys.exit(0)
