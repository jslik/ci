#!/usr/bin/env python

### IMPORTS ###
import sys, os
from functools import partial
from collections import defaultdict

sys.path.append(os.path.join(os.path.split(__file__)[0], '..', '..'))
from peas.methods.neat import NEATPopulation, NEATGenotype
from peas.visualization.neural_net_vis import visualize
# from peas.methods.neatpythonwrapper import NEATPythonPopulation
from peas.tasks.xor import XORTask
import peas.methods.hyperneat as hn

# Create a factory for genotypes (i.e. a function that returns a new 
# instance each time it is called)
genotype = lambda innovations: NEATGenotype(inputs=5,
                                weight_range=(-3., 3.),
                                types=['tanh'], innovations=innovations)

# Create a population
pop = NEATPopulation(genotype, popsize=20)

# Create a task
task = XORTask()

nodecounts = defaultdict(int)

gen = []
phen = []

for i in xrange(1):
    # Run the evolution, tell it to use the task as an evaluator
    pop.epoch(generations=300, evaluator=task, solution=task)#, converter=hn.create_converter(hn.Substrate((5, 2))))

    nodecounts[len(pop.champions[-1].node_genes)] += 1

    gen.append(list(pop.population))
    phen.append(list(pop.phenotypes))

json = {'generations':{'individuals': gen, 'phenotypes': phen}}
visualize(json, 'test')

print sorted(nodecounts.items())
