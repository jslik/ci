import os, sys, time
import pickle, psutil
import numpy as np
import csv

from ..networks.rnn import NeuralNetwork

locrace1 = '/home/jesper/Documents/Uni/CI/torcs-client/races/forza.xml'
locrace2 = '/home/jesper/Documents/Uni/CI/torcs-client/races/alpine-1.xml'
locrace3 = '/home/jesper/Documents/Uni/CI/torcs-client/races/alpine-2.xml'

locrace1_10 = '/home/jesper/Documents/Uni/CI/torcs-client/races/forza_10.xml'
locrace2_10 = '/home/jesper/Documents/Uni/CI/torcs-client/races/alpine-1_10.xml'


class TorcsTask(object):
    def __init__(self):
        self.nn_loc = '/home/jesper/Documents/Uni/CI/torcs-client/networks/nn_1.pickle'
        self.nn_loc = '/home/jesper/Documents/Uni/CI/torcs-client/networks/nn_'
        self.run_loc = '/home/jesper/Documents/Uni/CI/torcs-client/runAll.py'
        self.out_loc = '/home/jesper/Documents/Uni/CI/torcs-client/dumps/out_torcstask.txt'
        self.err_loc = '/home/jesper/Documents/Uni/CI/torcs-client/dumps/err_torcstask.txt'
        self.fitness_loc = '/home/jesper/Documents/Uni/CI/torcs-client/logs/0000'

    def evaluate(self, network, index):
        if not isinstance(network, NeuralNetwork):
            network = NeuralNetwork(network)

        network.make_feedforward()

        with open((self.nn_loc+'1.pickle'), 'wb') as file:
            pickle.dump(network, file=file)
            
        raceloc = [locrace1, locrace2, locrace3][index]

        eval_process = psutil.Popen(
            ['sudo', 'python3', self.run_loc, '-t', str(210), '-r', raceloc],
            stdout=open(self.out_loc, 'w'),
            stderr=open(self.err_loc, 'w')
        )
        eval_process.wait()

        with open(self.fitness_loc, 'r') as f:
            contents = csv.reader(f, delimiter='\t')
            for row in contents: # should be only 1 row in this method
                fit = max(float(row[1]), 1)

        print fit
            
        return {'fitness': fit}

    def evaluate_all(self, population, index):
        raceloc = [locrace1, locrace2, locrace3][max(index, 1)]
        read_fitness = {}
        for batch in xrange(len(population)/10):
            individuals = population[10*batch:10*(batch+1)]

            for i in xrange(1,11):
                network = NeuralNetwork(individuals[i-1])
                with open((self.nn_loc + str(i) + '.pickle'), 'wb') as file:
                    pickle.dump(network, file=file)

            eval_process = psutil.Popen(
                ['sudo', 'python3', self.run_loc, '-t', str(5), '-r', raceloc, '-n', str(10)],
                stdout=open(self.out_loc, 'w'),
                stderr=open(self.err_loc, 'w')
            )
            eval_process.wait()

            with open(self.fitness_loc, 'r') as f:
                contents = csv.reader(f, delimiter='\t')
                for row in contents:  # should be only 1 row in this method
                    read_fitness[float(row[0])+10*batch] = float(row[1])

        time.sleep(.5)
        os.system('sudo pkill -f .xml')
        os.system('sudo pkill -f run.py')
        time.sleep(.5)
        
        result = []
        for key in sorted(read_fitness):
            result.append(read_fitness[key])

        for i in xrange(len(population)):
            individual = population[i]
            individual.stats = {'fitness': result[i]}
            individual.stats['id'] = individual.id
            individual.stats['parent1'] = individual.parent1
            individual.stats['parent2'] = individual.parent2
            individual.stats['generation'] = individual.generation

    def solve(self, network):
        return False
