#! /usr/bin/env python3

import os, time
import psutil, argparse

locRun = '/home/jesper/Documents/Uni/CI/torcs-client/run.py'
locDumps = '/home/jesper/Documents/Uni/CI/torcs-client/dumps'
locRace1 = '/home/jesper/Documents/Uni/CI/torcs-client/races/quickrace.xml'
locRace2 = '/home/jesper/Documents/Uni/CI/torcs-client/races/quickrace2.xml'
locRace10 = '/home/jesper/Documents/Uni/CI/torcs-client/races/quickrace10.xml'
locNN = '/home/jesper/Documents/Uni/CI/torcs-client/networks/nn_'
locFitness = '/home/jesper/Documents/Uni/CI/torcs-client/logs/0000'
locStop = '/home/jesper/Documents/Uni/CI/torcs-client/dumps/stop'

out = '/home/jesper/Documents/Uni/CI/torcs-client/dumps/stdOut.txt'
err = '/home/jesper/Documents/Uni/CI/torcs-client/dumps/stdErr.txt'


def really_running(proc):
    """Check whether a process is running _and_ isn't a zombie"""
    return proc.is_running() and proc.status() != psutil.STATUS_ZOMBIE


def start(ncar, raceloc, evaltime=float("inf")):
    if os.path.isfile(locStop):
        os.remove(locStop)
    if os.path.isfile(locFitness):
        os.remove(locFitness)
    starttime = time.time()
    start_server(nCars=ncar, raceloc=raceloc)
    client_processes = []

    port = 3001
    for car in range(1, ncar+1):
        start_client(client_processes, port, evaltime, str(car), locNN+str(car)+'.pickle')
        port += 1

    stop = False
    while not stop:
        # check if everyone is ready
        if os.path.isfile(locFitness):
            with open(locFitness) as f:
                driversready = sum(1 for _ in f)
                stop = driversready == ncar

    print(time.time()-starttime)
    os.system('sudo pkill -f .xml')
    os.system('sudo pkill -f run.py')


def start_client(client_processes, port, time, id, locnn):
    print('starting client ' + id + ' at ' + str(port))
    driver_process = psutil.Popen(
        ['sudo', 'python3', locRun, '-p', str(port), '-t', str(time), '-i', id, '-n', locnn],
        stdout=open(locDumps+'/out'+str(port)+'.txt', 'w'),
        stderr=open(locDumps+'/err'+str(port)+'txt', 'w')
    )
    client_processes.append(driver_process)


def start_server(nCars, raceloc):
    return(psutil.Popen(
        ['sudo', 'torcs', '-r', raceloc],
        stdout=open(out, 'w'),
        stderr=open(err, 'w')
    ))


if __name__ == '__main__':
    # parser = argparse.ArgumentParser(
    #     description='Blabla'
    # )
    # parser.add_argument(
    #     '-t',
    #     '--time',
    #     help='Maximum simulation evaluation time in seconds (so simulation seconds)',
    #     type=int,
    #     default=9223372036854775807
    # )
    # parser.add_argument(
    #     '-r',
    #     '--race',
    #     help='Location of the race track',
    #     type=str,
    #     default=locRace1
    # )
    # parser.add_argument(
    #     '-n',
    #     '--ncar',
    #     help='Numbers of cars run simultaneously',
    #     type=int,
    #     default=1
    # )
    # args = parser.parse_args()
    #
    # start(ncar=args.ncar, evaltime=args.time, raceloc=args.race)
    start(ncar=10, evaltime=20, raceloc=locRace10)
