import os, sys, pickle, numpy as np

sys.path.insert(0, '/home/jesper/Documents/Uni/CI/torcs-client/NEAT/src')
from peas.networks.rnn import NeuralNetwork
from peas.methods.neat import NEATGenotype

nn_loc = '/home/jesper/Documents/Uni/CI/torcs-client/networks/best.pickle'
nn_loc2 = '/home/jesper/Documents/Uni/CI/torcs-client/networks/nn.pickle'
nn_loc3 = '/home/jesper/Documents/Uni/CI/torcs-client/results/run3_best_nn.pickle'

with open(nn_loc3, 'rb') as f:
    nn = pickle.load(file=f)
    # if not isinstance(nn, NeuralNetwork):
    #     nn = NeuralNetwork(nn)

print(nn)

input = [1] * 7

# self.feedforward = False
# self.sandwich = False
# self.cm = None
# self.node_types = None
# self.original_shape = None
# self.sum_all_node_inputs = False
# self.all_nodes_same_function = False

print(nn.feed(np.asarray(input)))

print(nn.cm_string())

print(np.round(nn.cm, 2))