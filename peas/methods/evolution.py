""" This module implements the different genotypes and evolutionary
    methods used in NEAT and HyperNEAT. It is meant to be a reference
    implementation, so any inneficiencies are copied as they are
    described.
"""

### IMPORTS ###
import sys
import random
import multiprocessing
from copy import deepcopy
from itertools import product
from collections import defaultdict

from ..networks.rnn import NeuralNetwork


# Libs
import numpy as np
np.seterr(over='warn', divide='raise')

# Package

# Shortcuts
rand = random.random
inf  = float('inf')


### CLASSES ###
                
class SimplePopulation(object):
    
    def __init__(self, geno_factory, 
                       popsize = 100, 
                       elitism = True,
                       stop_when_solved=False, 
                       tournament_selection_k=3,
                       verbose=True,
                       max_cores=1):
        # Instance properties
        self.geno_factory           = geno_factory
        self.popsize                = popsize
        self.elitism                = elitism
        self.stop_when_solved       = stop_when_solved
        self.tournament_selection_k = tournament_selection_k
        self.verbose                = verbose
        self.max_cores              = max_cores

        cpus = multiprocessing.cpu_count()
        use_cores = min(self.max_cores, cpus-1)
        if use_cores > 1:
            self.pool = multiprocessing.Pool(processes=use_cores, maxtasksperchild=5)
        else:
            self.pool = None

    def evaluate_individual(self, (individual, evaluator, phenotype)):
        try:
            NeuralNetwork(phenotype)
        except:
            ## Some weird situation that NEATGenotye cannot be converted to a nn
            ## Occurs nearly never, but once in ~100000 evals, for a presumably infeasible geno, and only whilst simulating dNEAT
            print 'Cannot convert NEATGenotype to nn! Assigning a fitness of 0.  evolution.py -> evaluate_individual()'
            individual.stats['id'] = -1
            individual.stats['parent1'] = -1
            individual.stats['parent2'] = -1
            individual.stats['generation'] = -1
            individual.stats['fitness'] = 0
            return individual

        if callable(evaluator):
            individual.stats = evaluator(phenotype)
        elif hasattr(evaluator, 'evaluate'):
            individual.stats = evaluator.evaluate(phenotype, index=self.trackindex)
        else:
            raise Exception("Evaluator must be a callable or object" \
                            "with a callable attribute 'evaluate'.")
        individual.stats['id'] = individual.id
        individual.stats['parent1'] = individual.parent1
        individual.stats['parent2'] = individual.parent2
        individual.stats['generation'] = individual.generation
        return individual

    def _reset(self):
        """ Resets the state of this population.
        """
        self.population      = [] # List of individuals
        self.phenotypes      = []
                
        # Keep track of some history
        self.champions  = []
        self.generation = 0
        self.solved_at  = None
        self.stats = defaultdict(list)
                
    def epoch(self, evaluator, generations, solution=None, reset=True, callback=None, converter=lambda x: x):
        """ Runs an evolutionary epoch 

            :param evaluator:    Either a function or an object with a function
                                 named 'evaluate' that returns a given individual's
                                 fitness.
            :param callback:     Function that is called at the end of each generation.
            :param converter:    Function that converts the neat genotype in another network
        """
        self.reset = reset
        if self.reset:
            self._reset()
        
        for _ in xrange(generations):
            self._evolve(evaluator, converter, solution)

            self.generation += 1

            if self.verbose:
                self._status_report()
            
            if callback is not None:
                callback(self)
                
            if self.solved_at is not None and self.stop_when_solved:
                break
                
        return {'stats': self.stats, 'champions': self.champions}
        
    def _evolve(self, evaluator, converter, solution=None):
        """ Runs a single step of evolution. """
        pop = self._birth()

        pop = self._evaluate_all(pop, evaluator, converter)
        self._find_best(pop, converter, solution)
        pop = self._reproduce(pop)
        self._gather_stats(pop)
                            
        self.population = pop

    def _birth(self):
        """ Creates a population if there is none, returns
            current population otherwise.
        """
        while len(self.population) < self.popsize:
            individual = self.geno_factory()
            self.population.append(individual)
        
        return self.population
        
    def _evaluate_all(self, pop, evaluator, converter):
        """ Evaluates all of the individuals in given pop,
            and assigns their "stats" property.
        """
        self.trackindex = random.choice([0,1])
        to_eval = [(individual, evaluator, converter(individual)) for individual in pop]
        self.phenotypes = [i[2] for i in to_eval]
        if self.run_multiple:
            evaluator.evaluate_all(pop, self.trackindex)
        else:
            if self.pool is not None:
                pop = self.pool.map(self.evaluate_individual, to_eval)
            else:
                pop = map(self.evaluate_individual, to_eval)
        
        return pop

    def _find_best(self, pop, converter, solution=None):
        """ Finds the best individual, and adds it to the champions, also 
            checks if this best individual 'solves' the problem.
        """
        ## CHAMPION
        self.champions.append(max(pop, key=lambda ind: ind.stats['fitness']))
        
        ## SOLUTION CRITERION
        if solution is not None:
            champion = converter(self.champions[-1])
            if isinstance(solution, (int, float)):
                solved = (champion.stats['fitness'] >= solution)
            elif callable(solution):
                solved = solution(champion)
            elif hasattr(solution, 'solve'):
                solved = solution.solve(champion)
            else:
                raise Exception("Solution checker must be a threshold fitness value,"\
                                "a callable, or an object with a method 'solve'.")
            
            if solved and self.solved_at is None:
                self.solved_at = self.generation

    def _reproduce(self, pop):
        """ Reproduces (and mutates) the best individuals to create a new population.
        """
        newpop = []

        if self.elitism:
            newpop.append(self.champions[-1])
            
        while len(newpop) < self.popsize:
            # Perform tournament selection
            k = min(self.tournament_selection_k, len(pop))
            winner = max(random.sample(pop, k), key=lambda ind:ind.stats['fitness'])
            winner = deepcopy(winner).mutate()
            newpop.append(winner)
            
        return newpop
        
    def _gather_stats(self, pop):
        """ Collects avg and max of individuals' stats (incl. fitness).
        """
        for key in pop[0].stats:
            if key in ['specie', 'id', 'generation', 'parent1', 'parent2']:
                continue
            self.stats[key+'_avg'].append(np.mean([ind.stats[key] for ind in pop]))
            self.stats[key+'_max'].append(np.max([ind.stats[key] for ind in pop]))
            self.stats[key+'_min'].append(np.min([ind.stats[key] for ind in pop]))
        self.stats['solved'].append( self.solved_at is not None )
        
    def _status_report(self):
        """ Prints a status report """
        print "\n== Generation %d ==" % self.generation
        print "Best (%.2f): %s %s" % (self.champions[-1].stats['fitness'], self.champions[-1], self.champions[-1].stats)
        print "Solved: %s" % (self.solved_at)
        